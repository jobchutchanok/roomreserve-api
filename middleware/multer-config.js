const multer = require('multer');
const path = require('path');
const fs = require('fs');
const sharp = require('sharp');

const uploadDir = path.join(__dirname, '..', 'uploads');

if (!fs.existsSync(uploadDir)) {
  fs.mkdirSync(uploadDir, { recursive: true });
}

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'uploads/');
  },
  filename: (req, file, cb) => {

    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
    cb(null, file.fieldname + '-' + uniqueSuffix + '.' + file.originalname.split('.').pop());
  },
});

const upload = multer({ storage: storage });

const resizeAndHandleImages = (req, res, next) => {
  const uploadedFiles = req.files;
  const delFile = req.body.delImg;
  // console.log('uploadedFiles', uploadedFiles);
  // console.log('body', req.body);
  // next();
  const resizedFileNames = [];
  if (delFile) {
    const filenamesToDelete = delFile.split(',');
    filenamesToDelete.forEach((filename) => {
      const filePath = path.join(uploadDir, filename);
      try {
        fs.unlinkSync(filePath);
        // console.log(`Deleted ${filename}`);
      } catch (err) {
        // console.error(`Error deleting ${filename}: ${err.message}`);
      }
    });
  }
  if (!uploadedFiles || uploadedFiles.length === 0) {
    // Handle the case where no files were uploaded
    // You can take appropriate action here, such as setting default data or returning an error
    // Example: req.defaultData = { key: 'value' };
    return next(); // Continue to the next middleware or route handler
  }
  uploadedFiles.forEach((file) => {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
    const resizedFileName = 'resized-' + file.fieldname + '-' + uniqueSuffix + '.' + file.originalname.split('.').pop();

    sharp(file.path)
      .resize({ width: 300, height: 200 })
      .toFile('uploads/' + resizedFileName, (err, info) => {
        if (err) {
          return res.status(500).send('Error resizing one or more files.');
        }
        // Remove the original file after resizing
        fs.unlinkSync(file.path);
        resizedFileNames.push(resizedFileName); // Store the resized file name
        if (resizedFileNames.length === uploadedFiles.length) {
          // Attach the resized file names to the response object
          req.resizedFileNames = resizedFileNames;
          next();
        }
      });
  });
};

module.exports = { upload, resizeAndHandleImages };
