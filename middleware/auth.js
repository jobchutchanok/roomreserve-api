const jwt = require('jsonwebtoken');
const secretKey = process.env.secret_key;

const AuthMiddleware = async (req, res, next) => {
  const token = req.headers.authorization;
  if (!token) {
    return res.status(401).json({ message: 'Invalid token' });
  }
  try {
    const decodedToken = jwt.verify(token.replace('Bearer ', ''), secretKey);
    req.user = decodedToken;
    // console.log('revokedTokens', revokedTokens)
    // return res.status(200).json(req.user);
    next();
  } catch (error) {
    // console.log('Invalid token:', error.message);
    // console.log(getUrl)
    // return res.redirect(getUrl);
    return res.status(401).json({ message: 'Invalid token' });
  }
};

module.exports = AuthMiddleware;
