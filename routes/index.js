const express = require('express');
const userRouter = require('./user');
const bodyParser = require('body-parser')
const adminRouter = require('./admin');
const router = express.Router();
// router.use(bodyParser.json());
// router.use(bodyParser.urlencoded({ extended: true }));
router.use('/', userRouter);
// router.use('/admin', adminRouter);


module.exports = (app) => {
    app.use(router);
    // app.use(bodyParser.json({ limit: '50mb' }));
    // app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }))
};