const express = require("express");
const router = express.Router();
const auth = require("../middleware/auth")
const Controller = require("../controllers")
const { upload, resizeAndHandleImages } = require("../middleware/multer-config")

router.get('/', (req, res) => {
    // Handle GET request for the homepage
    res.send('Welcome to the homepage!');
});

router.get('/login', Controller.LoginController.login)
// router.get('/callback', Controller.LoginController.callback)
// router.post('/logout', Controller.LoginController.logout)
// router.get('/decodeToken', Controller.LoginController.decode)

router.get('/rooms', Controller.RoomController.findAll)
router.get('/rooms/:slug', Controller.RoomController.findBySlug)


module.exports = router;