const models = require("../models")
const checkedUser = async (params) => {
    try {
        const findUser = await models.User.findOne({ where: { username: params.username } });
        if (!findUser || findUser.password !== params.password) {
            throw new Error('Invalid username or password');
        }
        return findUser;
    } catch (error) {
        throw error;
    }
};
module.exports = {
    checkedUser
}
