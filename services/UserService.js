const Model = require("../models")

const findAll = async (req, res, next) => {
    try {
        const params = req.query;
        const data = await Service.ToolService.findAll(params);
        res.status(200).send(data);
    } catch (error) {
        next(error);
    }
};
const create = async (req, res, next) => {
    try {
        const params = {
            body: req.body,
            imgs: req.resizedFileNames
        };
        const createTool = await Service.ToolService.create(params);
        res.status(200).send(createTool);
    } catch (error) {
        console.log(error)
        res.status(500).send('Error ');
    }
};
const update = async (req, res, next) => {
    try {
        let parsedOldImg;
        try {
            parsedOldImg = JSON.parse(req.body.oldImg);
        } catch (error) {
            parsedOldImg = req.body.oldImg ? [req.body.oldImg] : []; // You can set it to an empty array or another default value
        }
        const resizedFileNames = req.resizedFileNames ? req.resizedFileNames : [];
        // const parsedOldImg = JSON.parse(req.body.oldImg) ? JSON.parse(req.body.oldImg) : req.body.oldImg;
        const allImg = [...resizedFileNames, ...parsedOldImg];
        const params = {
            body: req.body,
            imgs: allImg
        };
        const updatedTool = await Service.ToolService.update(params);
        res.status(200).send(updatedTool);
    } catch (error) {
        next(error);
    }
};
const remove = async (req, res, next) => {
    // const params = req.params.id;
    // const imgs = req.body.img;
    try {
        // if (imgs.length > 0) {
        //     JSON.parse(imgs).forEach((filename) => {
        //         const filePath = path.join(uploadDir, filename);
        //         try {
        //             fs.unlinkSync(filePath);
        //             console.log(`Deleted ${filename}`);
        //         } catch (err) {
        //             console.error(`Error deleting ${filename}: ${err.message}`);
        //         }
        //     });
        // }
        const removeTools = await Service.ToolService.remove(req.params.id);
        // console.log(req.body)
        res.status(200).send(removeTools);
    } catch (error) {
        next(error);
    }
};
const findById = async (req, res, next) => {
    const params = req.params.id;
    try {
        const statusTool = await Service.ToolService.findByStore(params);
        res.status(200).send(statusTool);
    } catch (error) {
        next(error);
    }
};
module.exports = {
    findAll, create, update, remove, findById
}