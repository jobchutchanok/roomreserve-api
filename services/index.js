const RoomService = require('./RoomService');
const UserService = require('./UserService');
const LoginService = require('./LoginService');

// Export all models as properties of an object
module.exports = {
    RoomService,
    UserService,
    LoginService
};