function getOffset(currentPage = 1, listPerPage) {
    return (currentPage - 1) * [listPerPage];
}
function generateRandomNumber() {
    const min = 100000; // Minimum 6-digit number
    const max = 999999; // Maximum 6-digit number
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
let orderCounter = {};
const generateOrderNumberByDate = () => {
    const currentDate = new Date();
    const formattedDate = currentDate.toISOString().slice(0, 10).replace(/-/g, '');

    // Initialize counter for the current date if not exists
    if (!orderCounter[formattedDate]) {
        orderCounter[formattedDate] = 1;
    } else {
        // Increment the counter for the current date
        orderCounter[formattedDate]++;
    }

    // Create the order_number by combining date and counter
    const orderNumber = `${formattedDate}${orderCounter[formattedDate].toString().padStart(2, '0')}`;

    return orderNumber;
};

module.exports = {
    getOffset, generateRandomNumber, generateOrderNumberByDate
}

