const { Sequelize } = require('sequelize');
const config = require('./config');
const mysql = require('mysql2');

const env = process.env.NODE_ENV || 'development';
const dbConfig = config[env];
// const db2Config = config['student'];

const sequelize = new Sequelize(dbConfig.database, dbConfig.username, dbConfig.password, {
    host: dbConfig.host,
    // port: 9906,
    dialect: dbConfig.dialect,
    timezone: '+07:00', // Offset for Asia/Bangkok timezone
});
// const sequelize2 = new Sequelize(db2Config.database, db2Config.username, db2Config.password, {
//     host: db2Config.host,
//     // port: 9906,
//     dialect: db2Config.dialect,
// });
const pool = mysql.createPool({
    host: process.env.cmuagro_host,
    user: process.env.cmuagro_user,
    database: process.env.cmuagro_database,
    password: process.env.cmuagro_password,
    waitForConnections: true,
    connectionLimit: 10, // Adjust as needed
    queueLimit: 0 // Unlimited queueing
});

module.exports = {
    sequelize,
    //  sequelize2 
    pool
};
