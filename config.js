module.exports = {
    development: {
        username: 'root',
        password: '',
        database: 'agro_booking_rooms',
        host: 'localhost',
        dialect: 'mysql',
    },
    production: {
        username: process.env.USER,
        password: process.env.PASSWORD,
        database: process.env.DATABASE,
        host: process.env.HOST,
        dialect: 'mysql',
    },
}