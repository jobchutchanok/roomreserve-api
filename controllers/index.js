const RoomController = require('./RoomController');
const UserController = require('./UserController');
const LoginController = require('./LoginController');




// Export all models as properties of an object
module.exports = {
    RoomController,
    UserController,
    LoginController
};