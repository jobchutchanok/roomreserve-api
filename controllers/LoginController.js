const Service = require('../services/')
const jwt = require('jsonwebtoken');
const login = async (req, res) => {
    try {
        const checkedUser = await Service.LoginService.checkedUser(req.query);
        const token = jwt.sign({
            checkedUser
        }, process.env.secret_key, {
            expiresIn: '4h'
        });
        // Redirect to the client URL with the token as a query parameter
        res.redirect(`${process.env.FONT_URL}/?token=${token}`);
    } catch (error) {
        console.log(error)
        res.status(401).send('Invalid username or password');
    }
};
const callback = async (req, res) => {
    try {
        console.log(process.env.FONT_URL)
        // Generate a JWT token
        const token = jwt.sign({
            getUser
        }, secretKey, {
            expiresIn: '4h'
        });
        const decodedToken = jwt.verify(token.replace('Bearer ', ''), secretKey);
        // Redirect the client to a different route
        // res.redirect(`${process.env.FONT_URL}?token=${token}`);
        // Return the token as the response
        res.status(200).send({
            token,
            decodedToken
        });
    } catch (error) {
        // console.error('Error:', error);
        res.status(500).send('Internal Server Error');
    }
};
module.exports = {
    login,
    callback,
};