const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const { sequelize, pool } = require('./db');
const initRoutes = require('./routes/index');
require('dotenv').config();
require('./models');

const app = express();
const port = process.env.PORT || 3001;

// Middleware
app.use(cors());
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use((err, req, res, next) => {
    console.error(err.stack);
    res.status(500).send('Something broke!');
});

// Initialize routes
initRoutes(app);

// Database connection
async function connectToDatabase() {
    try {
        await sequelize.authenticate();
        console.log('Connection to the database has been established successfully.');
        await sequelize.sync({ force: false, alter: true });
        console.log('All models were synchronized successfully.');
    } catch (error) {
        console.error('Unable to connect to the database:', error);
        process.exit(1); // Exit the process if unable to connect to the database
    }
}

// Start the server
async function startServer() {
    await connectToDatabase();
    app.listen(port, () => {
        console.log(`Server is running on port ${port}`);
    });
}

startServer();

// Default route
app.get('/', (req, res) => {
    res.send('Hello World!');
});

app.get('/test', (req, res) => {
    pool.query('SELECT * from adm_admins', (err, results, fields) => {
        if (err) {
            console.error('Error executing query:', err);
            return;
        }
        res.send(results);
});
});
process.on('SIGINT', () => {
    pool.end((err) => {
        if (err) {
            console.error('Error closing pool:', err);
        }
        console.log('Pool has been closed.');
        process.exit();
    });
});