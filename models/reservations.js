const { DataTypes } = require('sequelize');
const { sequelize } = require('../db'); // Your sequelize instance

const Reservations = sequelize.define('reservations', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    room_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    user_id: {
        type: DataTypes.INTEGER,
        allowNull: true,
    },
    begin: {
        type: DataTypes.DATE,
        allowNull: true,
    },
    end: {
        type: DataTypes.DATE,
        allowNull: true,
    },
    approver: {
        type: DataTypes.INTEGER,
        allowNull: true,
    },
    approver_date: {
        type: DataTypes.DATE,
        allowNull: true,
    },
    approver_do_by: {
        type: DataTypes.INTEGER,
        allowNull: true,
    }
    // Add more fields as needed
});

module.exports = Reservations;