const User = require('./user');
const Room = require('./room')
const Reservations = require('./reservations')


module.exports = {
    User, Room, Reservations
};