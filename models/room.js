const { DataTypes } = require('sequelize');
const { sequelize } = require('../db'); // Your sequelize instance

const Room = sequelize.define('room', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    detail: {
        type: DataTypes.STRING,
        allowNull: true,
    },
    room_type: {
        type: DataTypes.STRING,
        allowNull: true,
    },
    slug: {
        type: DataTypes.STRING,
        allowNull: true,
    },
    ready_status: {
        type: DataTypes.ENUM('ready', 'waiting'),
        allowNull: true,
        defaultValue: 'ready'
    },
    role: {
        type: DataTypes.ENUM('normal', 'exclusive'),
        allowNull: true,
        defaultValue: 'normal'
    }
    // Add more fields as needed
});

module.exports = Room;